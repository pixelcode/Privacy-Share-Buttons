# Privacy Share Buttons
![](https://codeberg.org/pixelcode/Privacy-Share-Buttons/raw/commit/f19d26f80607aa7b644a2392457c5bd46a9aa621/Privacy%20Share%20Buttons.png)

These privacy-friendly HTML share buttons give the user the opportunity to share your blog articles etc. to social media without including any scripts or cookies from Facebook, Twitter, etc.


## Share to Mastodon
If the user wants to share your URL to Mastodon, he needs to enter his own instance first:
![](https://codeberg.org/pixelcode/Privacy-Share-Buttons/raw/commit/5b5a7b4145a7d2348af0283b428af0bc1ea39261/Choose%20mastodon%20instance.png)

![](https://codeberg.org/pixelcode/Privacy-Share-Buttons/raw/commit/d129b0dfddabd49af0f6d3d37498d1fe1879ae28/Mastodon%20example%20toot.png)

## Click to copy
Copying the URL via the black paperclip button will be confirmed like this:
![](https://codeberg.org/pixelcode/Privacy-Share-Buttons/raw/commit/5b5a7b4145a7d2348af0283b428af0bc1ea39261/Link%20copy%20success.png)

## How does it work?
Well, there's just an HTML table with some Font Awesome icons:

```
<table class="articleshare">
	<tr>
		<td class="teilentd" id="telegramteilentd">
			<a class="teilenbutton" id="telegramteilen" title="Telegram"><i class="fab fa-telegram"></i></a>
		</td>
		<td class="teilentd" id="whatsappteilentd">
			<a class="teilenbutton" id="whatsappteilen" title="WhatsApp"><i class="fab fa-whatsapp"></i></a>
		</td>
		<td class="teilentd" id="twitterteilentd">
			<a class="teilenbutton" id="twitterteilen" title="Twitter"><i class="fab fa-twitter"></i></a>
		</td>
		<td class="teilentd" id="mastodonteilentd">
			<a class="teilenbutton" id="mastodonteilen" title="Mastodon"><i class="fab fa-mastodon"></i></a>
		</td>
		<td id="mastodoninstanztd">
			<a id="mastodoninstanz"><i class="fab fa-mastodon"></i> Domain: <i id="https">https://</i><input id="mastodoninstanzinput" type="text" min-length="3" max-length="100" length="50" placeholder="mastodon.social"></input><i id="mastodoninstanzok">OK</i><i id="mastodoninstanzabbrechen" class="fas fa-times"></i></a>
		</td>
		<td class="teilentd" id="emailteilentd">
			<a class="teilenbutton" id="emailteilen" title="E-Mail"><i class="far fa-envelope"></i></a>
		</td>
		<td class="teilentd" id="linkteilentd">
			<a class="teilenbutton" id="linkteilen" title="Link"><i class="fas fa-link"></i></a>
		</td>
		<td id="linkkopierttd">
			<a class="teilenbutton" id="linkteilen2"><i class="far fa-check-circle"></i> Link has been copied!</a>
		</td>
	</tr>
</table>
```

When you click on one of these buttons, you'll be redirected to a URL of the respective social media service creating a new post with your website link and – if you want – a custom message like "Check out this great blog article".

```
$('#whatsappteilentd').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '%26');
	var titel = 'custom message | ';
	var nachricht = titel +  ': ' + link;
	window.location.href = 'whatsapp://send?text=' + nachricht;
});

$('#telegramteilentd').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '%26');
	var titel = 'custom message | ';
	window.location.href = 'https://t.me/share/url?url=' + link + '&text=' + titel;
});

$('#twitterteilentd').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '%26');
	var titel = 'custom message | ';
	var nachricht = titel +  ': ' + link;
	window.location.href = 'https://twitter.com/intent/tweet?text=' + nachricht;
});

$('#mastodonteilentd').click(function(){
	$('.teilentd').css({ 'display': 'none' });
	$('#datetd').css({ 'display': 'none' });
	$('#sharetd').css({ 'display': 'none' });
	$('#mastodoninstanztd').css({ 'display': 'inline-block' });
});

$('#mastodoninstanzok').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '%26');
	var titel = 'custom message | ';
	var nachricht = titel +  ': ' + link;
	var instanz = $('#mastodoninstanzinput').val();
	window.location.href = 'https://' + instanz + '/web/statuses/new?text=' + nachricht;
});

$('#mastodoninstanzabbrechen').click(function(){
	document.location.reload();
});

$('#emailteilentd').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '&amp;');
	var titel = 'custom message | ';
	var nachricht = titel +  ': ' + link;
	var nachricht = encodeURI(nachricht);
	window.location.href = 'mailto:?body=' + nachricht;
});
```

When you click on the paperclip icon, this function copies the current URL to your clipboard:

```
function urlkopieren(element) {
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val($(element).text()).select();
	document.execCommand("copy");
	$temp.remove();
}

$('#linkteilentd').click(function(){
	$('#urlcopyel').text(window.location.href);
	urlkopieren('#urlcopyel');
	$('.teilentd').css({ 'display': 'none' });
	$('#linkkopierttd').css({ 'display': 'inline-block' });
});
```

## Licence
**Privacy Share Buttons** use **[jQuery](https://jquery.com)**.

**Privacy Share Buttons** use **[Font Awesome](https://fontawesome.com)** icons. These may be used according to the [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) licence.

You may freely use **Privacy Share Buttons**' source code according to the [MIT Licence](https://codeberg.org/pixelcode/Privacy-Share-Buttons/src/branch/master/LICENCE.md) (slightly modified). A link to my repo as the original source would be great 😎