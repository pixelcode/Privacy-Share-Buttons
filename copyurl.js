function urlkopieren(element) {
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val($(element).text()).select();
	document.execCommand("copy");
	$temp.remove();
}

$('#linkteilentd').click(function(){
	$('#urlcopyel').text(window.location.href);
	urlkopieren('#urlcopyel');
	$('.teilentd').css({ 'display': 'none' });
	$('#linkkopierttd').css({ 'display': 'inline-block' });
});