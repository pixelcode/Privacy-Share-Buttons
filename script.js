$('#whatsappteilentd').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '%26');
	var titel = 'custom message | ';
	var nachricht = titel +  ': ' + link;
	window.location.href = 'whatsapp://send?text=' + nachricht;
});

$('#telegramteilentd').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '%26');
	var titel = 'custom message | ';
	window.location.href = 'https://t.me/share/url?url=' + link + '&text=' + titel;
});

$('#twitterteilentd').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '%26');
	var titel = 'custom message | ';
	var nachricht = titel +  ': ' + link;
	window.location.href = 'https://twitter.com/intent/tweet?text=' + nachricht;
});

$('#mastodonteilentd').click(function(){
	$('.teilentd').css({ 'display': 'none' });
	$('#datetd').css({ 'display': 'none' });
	$('#sharetd').css({ 'display': 'none' });
	$('#mastodoninstanztd').css({ 'display': 'inline-block' });
});

$('#mastodoninstanzok').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '%26');
	var titel = 'custom message | ';
	var nachricht = titel +  ': ' + link;
	var instanz = $('#mastodoninstanzinput').val();
	window.location.href = 'https://' + instanz + '/web/statuses/new?text=' + nachricht;
});

$('#mastodoninstanzabbrechen').click(function(){
	document.location.reload();
});

$('#emailteilentd').click(function(){
	var link = window.location.href;
	var link = link.replace('&', '&amp;');
	var titel = 'custom message | ';
	var nachricht = titel +  ': ' + link;
	var nachricht = encodeURI(nachricht);
	window.location.href = 'mailto:?body=' + nachricht;
});